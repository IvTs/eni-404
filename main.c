#include<ioavr.h>
#include<intrinsics.h>
#include <comp_a90.h>                 

#define HC_DATACOMMAND PORTC_Bit5
#define DE PORTC_Bit2
#define RE PORTC_Bit1
#define RESET_HC PORTD_Bit3

#define SizeRxBuf 50

void UartIniRecieve(void); //Инициализация Uart
void UartIniTransmitRecieve(void); //Инициализация Uart
void FindATComand(void); //Поиск АТ команды
void SendSTR(unsigned char *data);
void SendCRLF(void);
void SendAnswer(void); //Отправить ответ полученный от модуля по бл
unsigned char EEPROM_WR(unsigned char AddrH, unsigned char Data); //Запись в ЕЕПРОМ
unsigned char EEPROM_READ(unsigned char AddrH); //Чтение в ЕЕПРОМ
void SendSTRHELP(char __flash *data1);

__flash char message1[]= {"AT  - Test command"};
__flash char message2[]={"AT+RESET  -  Reset Bluetooth Module"};
__flash char message3[]={"AT+VERSION? - Get soft version"};
__flash char message4[]={"AT+ORGL - Restore default settings"};
__flash char message5[]={"  1. Baud Rate: 38400 bit/s; Stop bit: 1 bit; Parity bit: None"};
__flash char message6[]={"  2. Passkey: 1234"};
__flash char message7[]={"  3. Device name: EnI-404"};
__flash char message8[]={"AT+ADDR?=  - Get module Bluetooth address"};
__flash char message9[]={"AT+NAME? - Get device's name"};
__flash char message10[]={"AT+NAME=<Param> - Set device's name"};
__flash char message11[]={"AT+PSWD? - Get passkey"};
__flash char message12[]={"AT+PSWD=<Param> - Set passkey"};
__flash char message13[]={"AT+UART? - Get SerialPort parameter" };
__flash char message14[]={"AT+UART=<Param1>,<Param2>,<Param3> - Set SerialPort parameter" };
__flash char message15[]={"  <Param1> - BaudRate: 4800 9600 19200 38400 57600 115200" };
__flash char message16[]={"  <Param2> - Stop bit: 0 - 1 Stop bit; 1 - 2 Stop bit" };
__flash char message17[]={"  <Param3> - Parity bit: 0 - None; 1 - Odd parity; 2 - Even parity" };


unsigned char RxBuf[SizeRxBuf], //Приемные буфер
RxCount = 0, //Счетчик символов в буфере
RxTile = 0, //Указатель конца буфера
RxHead = 0, //Указатель начала буфера
NumberCommand = 0, //Номер АТ-команды
CRflag = 0, //Флаг приема символа возврата каретки
RS485off = 0, //Флаг включения/выключения 485 интерфейса
ParamBuf[50], //Буфер вводимых в АТ команде параметров 
j = 0, //Индекс в буфере ParamBuf,
BaudRate = 0,
ParityBit = 0,
StopBit = 0,
BaudRateCurrent = 0, //Текущая скорость 
ParityBitCurrent = 0,
StopBitCurrent = 0,
i = 0;;



void main( void )
{
  DDRD_Bit2 = 0x00; 
  
  PORTD_Bit1 = 0x01;
  
  DDRC_Bit2 = 0x01; // DE = 0; 
  DDRC_Bit1 = 0x01; // RE = 1;
  
  DDRC_Bit5 = 0x01; //Переключает НС коммандный режим лог. 1/данные  лог. 0
  
///  DDRB_Bit0 = 0xff;//RESET_HC
 
  
  DDRD_Bit3 = 0x01;//RESET_HC 
  RESET_HC = 0x01;
  
  // В момент запуска необходимо HC отправить в командный режим на несколько сек, тогда он запустится на стандартной скорости 38400
  HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
  
  // Настройка прерываний по изменению уровня на INT0
  MCUCR |= (1<<ISC01); // Прерывания по заднему фронту имп.
  MCUCR &=~(1<<ISC00); // на входе INT0
  GICR|=(1<<INT0); //вкл внешние прерывания 

  if(EEPROM_READ(3) == 0xff)	// Если ЕЕПРОМ пуст, то записываем начальные параметры скорость 38400, 1 стоп бит, без контроля четности
  {
   while(EEPROM_WR(3,4)!=1);
   while(EEPROM_WR(4,0)!=1);
   while(EEPROM_WR(5,0)!=1);
  }

  __delay_cycles(11000000);
  __delay_cycles(11000000);
  __delay_cycles(11000000);

  // Настройка скорости HC-05
  BaudRateCurrent = 4;			// Настраиваем UART на 38400, чтобы назначить НС-05 скорость из ЕЕПРОМ
  StopBitCurrent = 0;  
  ParityBitCurrent = 0;
  __enable_interrupt();
  
  UartIniTransmitRecieve();//Включаем UART на передачу 
  
  unsigned char temp = 0; //выкл RS485
  temp = PINC;
  temp |=(1<<PC1);
  temp &=~(1<<PC2);
  PORTC = temp;  
  
  RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
  __delay_cycles(110000);
  
  BaudRateCurrent = EEPROM_READ(3);
  StopBitCurrent = EEPROM_READ(4);  
  ParityBitCurrent = EEPROM_READ(5);
  
  switch(BaudRateCurrent)	// Отправляем НС-05 команду на изменение скорости
  {
  case 0:
    {} break;
  case 1:
    { SendSTR("AT+UART=4800,"); } break;
  case 2:
    { SendSTR("AT+UART=9600,"); } break;
  case 3:
    { SendSTR("AT+UART=19200,");} break;
  case 4:
    { SendSTR("AT+UART=38400,");} break;
  case 5:
    { SendSTR("AT+UART=57600,");} break;
  case 6:
    { SendSTR("AT+UART=115200,");} break;
  }
  
  if(StopBitCurrent==0) {SendSTR("0,");}
  else if(StopBitCurrent==1)  {SendSTR("1,");}
  
  if (ParityBitCurrent==0) {SendSTR("0");}
  else if (ParityBitCurrent==1) {SendSTR("1");}
  else if (ParityBitCurrent==2) {SendSTR("2");}
  
  SendCRLF();
  __delay_cycles(11000000);
  
  HC_DATACOMMAND = 0; //Переключает НС в режим данные
  
  RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
  PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
  
  RESET_HC = 0;
  __delay_cycles(11000000);
  RESET_HC = 1;  
  //
  
  //Перестраиваем скорость UART с 38400 на скорость записанную в ЕЕПРОМ
  BaudRateCurrent = EEPROM_READ(3);
  StopBitCurrent = EEPROM_READ(4);  
  ParityBitCurrent = EEPROM_READ(5);
  UartIniRecieve(); //Инициализация UART на прием + прерывания по приему
  __enable_interrupt();


  //PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //На прием RS - 485 из 404 в бл
  //PORTC = PINC|(1<<PC1)|(1<<PC2);   // RS - 485 на передачу В 404 ПО БЛ
      
  while(1)
  {

   FindATComand(); //Ищем в полученных данных АТ-команды 
   
   switch(NumberCommand) //Обработчик АТ-команд
   {// Начало обработчика АТ команд
   case 1: // AT
     
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT :OK"); 
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
       
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(5000000); // 1100000
       
       SendSTR("AT"); //Посылаем в модуль команду
       SendCRLF();
        __enable_interrupt();
       __delay_cycles(11000000); //11000000
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100);  //11000000
       
       SendAnswer(); //Отправить по бл ответ полученый от НС
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
       
     } break;
     
   case 2: // AT+RESET
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+RESET :OK");
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       
       SendSTR("AT+RESET"); //Посылаем в модуль команду
       SendCRLF();
       UartIniRecieve();//Включаем UART на передачу 
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100000); 
       
       __enable_interrupt();
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
       /*
       RESET_HC = 0;
       __delay_cycles(1100000);
       RESET_HC = 1;
       */
     } break;
     
   case 3:// AT+NAME?
     {
        NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+NAME? :OK"); 
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
       
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       SendSTR("AT+NAME?"); //Посылаем в модуль команду
       SendCRLF();
       __enable_interrupt();
       __delay_cycles(11000000); 
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100000);  
       
       SendAnswer(); //Отправить по бл ответ полученый от НС
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
     }  break;
     
     case 4:// AT+ADDR?
     {
        NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+ADDR? :OK"); 
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
       
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       SendSTR("AT+ADDR?"); //Посылаем в модуль команду
       SendCRLF();
       __enable_interrupt();
       __delay_cycles(11000000); 
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100000);  
       
       SendAnswer(); //Отправить по бл ответ полученый от НС
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
     }  break;
     
      case 5:// AT+UART?
     {
        NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+UART? :OK"); 
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
       
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       SendSTR("AT+UART?"); //Посылаем в модуль команду
       SendCRLF();
       __enable_interrupt();
       __delay_cycles(11000000); 
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100000);  
       
       SendAnswer(); //Отправить по бл ответ полученый от НС
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
     }  break;
     
     case 6: // AT+NAME=<Param>
     
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+NAME<Param> :OK");
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
      
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       
       
       SendSTR("AT+NAME=");
       for(unsigned char i = 0; i!=j; i++) //Выводим все символы из массива Param
       {
         while(!(UCSRA & (1<<UDRE))); // Ждем пока освободится передатчик
         __delay_cycles(11000); //Задержка между посылкой символов 
         if(ParamBuf[i] != 0x0d) //Символ возврата каретки отбрасываем
         UDR = ParamBuf[i];
       }
       SendCRLF();
       
       __enable_interrupt();
       __delay_cycles(1100000);
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(11000000); 
 
       SendAnswer(); //Отправить по бл ответ полученый от НС
       
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
       
     } break;
     
     case 7: // AT+UART=<Param>,<Param1>,<Param2>
     
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+UART=<Param1>,<Param2>,<Param3> :OK");
       SendCRLF();
       
       switch(BaudRate)
       {
       case 0:
         {} break;
       case 1:
         { SendSTR("AT+UART=4800,"); while(EEPROM_WR(3,1)!=1); } break;
       case 2:
         { SendSTR("AT+UART=9600,"); while(EEPROM_WR(3,2)!=1); } break;
       case 3:
         { SendSTR("AT+UART=19200,"); while(EEPROM_WR(3,3)!=1); } break;
       case 4:
         { SendSTR("AT+UART=38400,"); while(EEPROM_WR(3,4)!=1); } break;
       case 5:
         { SendSTR("AT+UART=57600,"); while(EEPROM_WR(3,5)!=1); } break;
       case 6:
         { SendSTR("AT+UART=115200,"); while(EEPROM_WR(3,6)!=1); } break;
       }
      
       
       if(StopBit=='0') {SendSTR("0,"); while(EEPROM_WR(4,0)!=1);}
       else if(StopBit=='1')  {SendSTR("1,"); while(EEPROM_WR(4,1)!=1);}
       
       if (ParityBit=='0') {SendSTR("0");while(EEPROM_WR(5,0)!=1);}
       else if (ParityBit=='1') {SendSTR("1"); while(EEPROM_WR(5,1)!=1);}
       else if (ParityBit=='2') {SendSTR("2"); while(EEPROM_WR(5,2)!=1);}
       SendCRLF();
       
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
      
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       
       
       switch(BaudRate)
       {
       case 0:
         {} break;
       case 1:
         { SendSTR("AT+UART=4800,");  BaudRateCurrent = 1; } break;
       case 2:
         { SendSTR("AT+UART=9600,");  BaudRateCurrent = 2;} break;
       case 3:
         { SendSTR("AT+UART=19200,");  BaudRateCurrent = 3;} break;
       case 4:
         { SendSTR("AT+UART=38400,");  BaudRateCurrent = 4;} break;
       case 5:
         { SendSTR("AT+UART=57600,");  BaudRateCurrent = 5;} break;
       case 6:
         { SendSTR("AT+UART=115200,"); BaudRateCurrent = 6;} break;
       }
      
       
       if(StopBit=='0') {SendSTR("0,"); StopBitCurrent = 0;}
       else if(StopBit=='1')  {SendSTR("1,"); StopBitCurrent = 1;}
       
       if (ParityBit=='0') {SendSTR("0"); ParityBitCurrent = 0;}
       else if (ParityBit=='1') {SendSTR("1"); ParityBitCurrent = 1;}
       else if (ParityBit=='2') {SendSTR("2"); ParityBitCurrent = 2;}
       SendCRLF();
       
       UartIniTransmitRecieve();//Включаем UART на передачу 
       __delay_cycles(11000000);
       __delay_cycles(11000000); 
 


       HC_DATACOMMAND = 0; //Переключает НС в режим данные
       UartIniRecieve();//Включаем UART на передачу 
       __delay_cycles(1100000); 
       
       __enable_interrupt();
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
       
       RESET_HC = 0;
       __delay_cycles(1100000);
       RESET_HC = 1; 
     } break;
     
   //AT+ROLE?  
   case 8:
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+ROLE? :OK"); 
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
       
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       SendSTR("AT+ROLE?"); //Посылаем в модуль команду
       SendCRLF();
       __enable_interrupt();
       __delay_cycles(11000000); 
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100000);  
       
       SendAnswer(); //Отправить по бл ответ полученый от НС
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
     } break;
     
     //AT+PSWD?  
   case 9:
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+PSWD? :OK"); 
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
       
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       SendSTR("AT+PSWD?"); //Посылаем в модуль команду
       SendCRLF();
       __enable_interrupt();
       __delay_cycles(11000000); 
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100000);  
       
       SendAnswer(); //Отправить по бл ответ полученый от НС
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
     } break;
     
     // AT+PSWD=<Param>
      case 10: 
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+PSWD=<Param> :OK");
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
      
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       
       
       SendSTR("AT+PSWD=");
       for(unsigned char i = 0; i!=j; i++) //Выводим все символы из массива Param
       {
         while(!(UCSRA & (1<<UDRE))); // Ждем пока освободится передатчик
         __delay_cycles(11000); //Задержка между посылкой символов 
         if(ParamBuf[i] != 0x0d) //Символ возврата каретки отбрасываем
         UDR = ParamBuf[i];
       }
       SendCRLF();
       
       __enable_interrupt();
       __delay_cycles(1100000);
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(11000000); 
 
       SendAnswer(); //Отправить по бл ответ полученый от НС
       
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
       
     } break;
     
     //AT+VERSION?  
   case 11:
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+VERSION? :OK"); 
       SendCRLF();
       SendSTR("Rev. 02.05.0000"); //Версия прошивки МЕГИ
       SendCRLF();
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
       
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       SendSTR("AT+VERSION?"); //Посылаем в модуль команду
       SendCRLF();
       
       __enable_interrupt();
       __delay_cycles(11000000); 
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100000);  
       
       SendAnswer(); //Отправить по бл ответ полученый от НС
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
     } break;
     
     case 12: // AT+ORGL
     
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+ORGL :OK"); 
       SendCRLF();
       
       __disable_interrupt();
       unsigned char temp = 0; //выкл RS485
       temp = PINC;
       temp |=(1<<PC1);
       temp &=~(1<<PC2);
       PORTC = temp;  
       
       RS485off = 1; //Выключение RS 485 чтоб не мешался(в прерываниях ИНТ0)
       
       HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
       
       SendSTR("AT+ORGL"); //Посылаем в модуль команду
       SendCRLF();
        __enable_interrupt();
       __delay_cycles(11000000); 
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100000);  
       
       BaudRateCurrent = 4;
       StopBitCurrent = 0;
       ParityBitCurrent = 0;
       
       EEPROM_WR(0,2);
       EEPROM_WR(2,0);
       EEPROM_WR(1,0);
       
       __disable_interrupt();
       SendAnswer(); //Отправить по бл ответ полученый от НС
       
        HC_DATACOMMAND = 1; //Переключает НС в коммандный режим 
       __delay_cycles(1100000);
        SendSTR("AT+NAME=EnI-404"); //Посылаем в модуль команду
       SendCRLF();
       
       SendSTR("AT+RESET"); //Посылаем в модуль команду
       SendCRLF();
       
        __enable_interrupt();
       __delay_cycles(11000000); 
       HC_DATACOMMAND = 0; //Переключает НС в режим данные 
       __delay_cycles(1100000);  
       
       UartIniRecieve();//Включаем UART на передачу 
       
       RS485off = 0; //Включение RS 485 (в прерываниях ИНТ0)
       PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
       
       RESET_HC = 0;
       __delay_cycles(1100000);
       RESET_HC = 1;
       
     } break;
     
     case 13: // AT+HELP
     
     {
       NumberCommand = 0;
       UartIniTransmitRecieve();//Включаем UART на передачу 
       SendSTR("AT+HELP :OK"); 
       SendCRLF();SendCRLF();
       
       SendSTR("AT command set"); 
       SendCRLF();SendCRLF();
       
       SendSTRHELP(message1); 
       SendCRLF();
       
       SendSTRHELP(message2);
       SendCRLF();
       
       SendSTRHELP(message3);
       SendCRLF();
       
       SendSTRHELP(message4);
       SendCRLF();
       
       SendSTRHELP(message5);
       SendCRLF();
       
       SendSTRHELP(message6);
       SendCRLF();
       
       SendSTRHELP(message7);
       SendCRLF(); 
       
       SendSTRHELP(message8);
       SendCRLF(); 
       
       SendSTRHELP(message9);
       SendCRLF(); 
       
       SendSTRHELP(message10);
       SendCRLF(); 
       
       SendSTRHELP(message11);
       SendCRLF(); 
       
       SendSTRHELP(message12);
       SendCRLF(); 
       
       SendSTRHELP(message13);
       SendCRLF(); 
       
       SendSTRHELP(message14);
       SendCRLF(); 
       
       SendSTRHELP(message15);
       SendCRLF(); 
       
       SendSTRHELP(message16);
       SendCRLF(); 
       
       SendSTRHELP(message17);
       SendCRLF(); SendCRLF();
       
       UartIniRecieve();//Включаем UART на передачу 
 
     } break;
     
   }// Конец обработчика АТ команд
  
  }//Конец цикла while(1)
  
}

//======================================
//==Инициализация UART только на прием==
//======================================
void UartIniRecieve(void)
{
  switch(BaudRateCurrent)
  {
  case 1:
    {
      UBRRH=0x00; //скорость 4800
      UBRRL=143;
    } break;
  case 2:
    {
      UBRRH=0x00; //скорость 9600
      UBRRL=71;
    } break;
  case 3:
    {
      UBRRH=0x00; //скорость 19200
      UBRRL=35;
    } break;
  case 4:
    {
      UBRRH=0x00; //скорость 38400
      UBRRL=17;
    } break;
  case 5:
    {
      UBRRH=0x00; //скорость 57600
      UBRRL=11;
    } break;
  case 6:
    {
      UBRRH=0x00; //скорость 115200
      UBRRL=5;
    } break;
  }
  

  
  UCSRA=0x00; // без удвоения скорости
  UCSRB=0x10; // разрешили прием
  UCSRB|=(1<<RXCIE); //прерывания по приему

  switch(StopBitCurrent)
  {
  case 0:// 1-stopbit
    {
      switch(ParityBitCurrent)
      {
      case 0:
        { UCSRC =((1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1));} break;  // 1-stopbit, none parity
      case 2:
        {UCSRC =((1<<URSEL)|(1<<UPM1)|(1<<UCSZ0)|(1<<UCSZ1));} break; // 1-stopbit, even parity 
      case 1:
        {UCSRC =((1<<URSEL)|(1<<UPM1)|(1<<UPM0)|(1<<UCSZ0)|(1<<UCSZ1));} break; // 1-stopbit, Odd parity
      } 
  
    } break; // 1-stopbit
    
  case 1:// 2-stopbit
    {
      switch(ParityBitCurrent)
      {
      case 0:
        {UCSRC =((1<<URSEL)|(1<<USBS)|(1<<UCSZ0)|(1<<UCSZ1)); } break; // 2-stopbit, none parity
      case 2:
        {UCSRC =((1<<URSEL)|(1<<USBS)|(1<<UPM1)|(1<<UCSZ0)|(1<<UCSZ1));} break; // 2-stopbit, even parity // even parity
      case 1:
        {UCSRC =((1<<URSEL)|(1<<USBS)|(1<<UPM1)|(1<<UPM0)|(1<<UCSZ0)|(1<<UCSZ1));} break; // 2-stopbit, Odd parity
      } 
    } break; // 2-stopbit
  }
  

}

//==========================================
//==Инициализация UART на прием и передачу==
//==========================================
void UartIniTransmitRecieve(void) //Инициализация Uart
{
    switch(BaudRateCurrent)
  {
  case 1:
    {
      UBRRH=0x00; //скорость 4800
      UBRRL=143;
    } break;
  case 2:
    {
      UBRRH=0x00; //скорость 9600
      UBRRL=71;
    } break;
  case 3:
    {
      UBRRH=0x00; //скорость 19200
      UBRRL=35;
    } break;
  case 4:
    {
      UBRRH=0x00; //скорость 38400
      UBRRL=17;
    } break;
  case 5:
    {
      UBRRH=0x00; //скорость 57600
      UBRRL=11;
    } break;
  case 6:
    {
      UBRRH=0x00; //скорость 115200
      UBRRL=5;
    } break;
  }
        
   UCSRA=0x00; // без удвоения скорости
   UCSRB=0x18; // разрешили прием
   UCSRB|=(1<<RXCIE);//|(1<<TXCIE); //прерывания по приему и передаче
   
   switch(StopBitCurrent)
  {
  case 0:// 1-stopbit
    {
      switch(ParityBitCurrent)
      {
      case 0:
        {UCSRC =((1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1));} break;  // 1-stopbit, none parity
      case 2:
        {UCSRC =((1<<URSEL)|(1<<UPM1)|(1<<UCSZ0)|(1<<UCSZ1));} break; // 1-stopbit, even parity 
      case 1:
        {UCSRC =((1<<URSEL)|(1<<UPM1)|(1<<UPM0)|(1<<UCSZ0)|(1<<UCSZ1));} break; // 1-stopbit, Odd parity
      } 
  
    } break; // 1-stopbit
    
  case 1:// 2-stopbit
    {
      switch(ParityBitCurrent)
      {
      case 0:
        {UCSRC =((1<<URSEL)|(1<<USBS)|(1<<UCSZ0)|(1<<UCSZ1)); } break; // 2-stopbit, none parity
      case 2:
        {UCSRC =((1<<URSEL)|(1<<USBS)|(1<<UPM1)|(1<<UCSZ0)|(1<<UCSZ1));} break; // 2-stopbit, even parity // even parity
      case 1:
        {UCSRC =((1<<URSEL)|(1<<USBS)|(1<<UPM1)|(1<<UPM0)|(1<<UCSZ0)|(1<<UCSZ1));} break; // 2-stopbit, Odd parity
      } 
    } break; // 2-stopbit
  }
 
}

//===========================
//==ВНЕШНЕЕ ПРЕРЫВАНИЕ ИНТ0==
//===========================
//Срабатывает когда приходит стартовый бит
#pragma vector=INT0_vect
__interrupt void EX_INT0(void)
{
  if(RS485off==0)
  {PORTC = PINC|(1<<PC1)|(1<<PC2);}   // RS - 485 на передачу
}

//================================
//==Прерывания по приему символа==
//================================
#pragma vector=USART_RXC_vect
__interrupt void US_RXC(void)
{ 
  
  unsigned char i = UDR;
  if(i == 0x0d) CRflag = 1; // Флаг пришел символ перевода каретки
  PORTC = (PINC & (~((1<<PC2)|(1<<PC1)))); //RS - 485 на прием
  
  if(RxTile != SizeRxBuf) //Если есть место в приемном буфере
  {
    RxBuf[RxTile] = i; //Пишем принятый символ в конец буфера
    RxTile += 1; //Конец буфера +1
      
    if(RxTile == SizeRxBuf) RxTile = 0; //Если дошли до конца буфера начинаем писать все сначала
  } 
  
}

//===========================================
//==Поиск АТ-команд среди принятых символов==
//===========================================
void FindATComand(void)
{
  if(CRflag) //Если был принят символ возврата каретки
  {
    while(RxHead != RxTile) // Перебираем все принятые символы
    {   
      if((RxBuf[RxHead]=='A')&&(RxBuf[RxHead+1]=='T')) //Ищем символы АТ
      {//Распознаем название комманды и присваиваем ей номер
        // АТ
        if(RxBuf[RxHead+2]==0x0d) {NumberCommand = 1;} 
        //AT+RESET
        if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='R')&&(RxBuf[RxHead+4]=='E')&&\
          (RxBuf[RxHead+5]=='S')&&(RxBuf[RxHead+6]=='E')&&(RxBuf[RxHead+7]=='T')&&\
            (RxBuf[RxHead+8]==0x0d)) {NumberCommand = 2;} 
        //AT+NAME?
        if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='N')&&(RxBuf[RxHead+4]=='A')&&\
          (RxBuf[RxHead+5]=='M')&&(RxBuf[RxHead+6]=='E')&&(RxBuf[RxHead+7]=='?')&&\
            (RxBuf[RxHead+8]==0x0d)) {NumberCommand = 3;} 
        //AT+ADDR?
          if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='A')&&(RxBuf[RxHead+4]=='D')&&\
          (RxBuf[RxHead+5]=='D')&&(RxBuf[RxHead+6]=='R')&&(RxBuf[RxHead+7]=='?')&&\
            (RxBuf[RxHead+8]==0x0d)) {NumberCommand = 4;} 
        //AT+UART?
          if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='U')&&(RxBuf[RxHead+4]=='A')&&\
          (RxBuf[RxHead+5]=='R')&&(RxBuf[RxHead+6]=='T')&&(RxBuf[RxHead+7]=='?')&&\
            (RxBuf[RxHead+8]==0x0d)) {NumberCommand = 5;} 
        //AT+NAME=
          if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='N')&&(RxBuf[RxHead+4]=='A')&&\
          (RxBuf[RxHead+5]=='M')&&(RxBuf[RxHead+6]=='E')&&(RxBuf[RxHead+7]=='=')) 
          {
            j = 0; //Обнуляем индекс массива Param
            i=RxHead+8; //Задаем начальный индекс
            while(i<RxTile) //Считываем все принятые символы
            {  
              ParamBuf[j] = RxBuf[i]; //Заполняем массим Param
              j++; //Инкремент индексов
              i++;
            }
            NumberCommand = 6; //Задаем номер команды  
          } 
        
        //AT+UART=
          if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='U')&&(RxBuf[RxHead+4]=='A')&&\
          (RxBuf[RxHead+5]=='R')&&(RxBuf[RxHead+6]=='T')&&(RxBuf[RxHead+7]=='=')) 
          {
            //4800
            if((RxBuf[RxHead+8]=='4')&&(RxBuf[RxHead+9]=='8')&&(RxBuf[RxHead+10]=='0')&&\
            (RxBuf[RxHead+11]=='0')&&(RxBuf[RxHead+12]==',')&&(RxBuf[RxHead+14]==',')&&(RxBuf[RxHead+16]==0x0d))
            {
              if(((RxBuf[RxHead+13]=='0')||(RxBuf[RxHead+13]=='1'))&&((RxBuf[RxHead+15]=='0')||(RxBuf[RxHead+15]=='1')||(RxBuf[RxHead+15]=='2')))
              {
                NumberCommand = 7; //Задаем номер команды 
                BaudRate = 1; //4800
                ParityBit = RxBuf[RxHead+15];
                StopBit = RxBuf[RxHead+13];
              } 
            }
            //9600
            if((RxBuf[RxHead+8]=='9')&&(RxBuf[RxHead+9]=='6')&&(RxBuf[RxHead+10]=='0')&&\
            (RxBuf[RxHead+11]=='0')&&(RxBuf[RxHead+12]==',')&&(RxBuf[RxHead+14]==',')&&(RxBuf[RxHead+16]==0x0d))
            {
              if(((RxBuf[RxHead+13]=='0')||(RxBuf[RxHead+13]=='1'))&&((RxBuf[RxHead+15]=='0')||(RxBuf[RxHead+15]=='1')||(RxBuf[RxHead+15]=='2')))
              {
                NumberCommand = 7; //Задаем номер команды 
                BaudRate = 2; //9600
                ParityBit = RxBuf[RxHead+15];
                StopBit = RxBuf[RxHead+13];
              } 
            }
            //19200
            if((RxBuf[RxHead+8]=='1')&&(RxBuf[RxHead+9]=='9')&&(RxBuf[RxHead+10]=='2')&&\
            (RxBuf[RxHead+11]=='0')&&(RxBuf[RxHead+12]=='0')&&(RxBuf[RxHead+13]==',')&&(RxBuf[RxHead+15]==',')&&(RxBuf[RxHead+17]==0x0d))
            {
              if(((RxBuf[RxHead+14]=='0')||(RxBuf[RxHead+14]=='1'))&&((RxBuf[RxHead+16]=='0')||(RxBuf[RxHead+16]=='1')||(RxBuf[RxHead+16]=='2')))
              {
                NumberCommand = 7; //Задаем номер команды 
                BaudRate = 3; //19200
                ParityBit = RxBuf[RxHead+16];
                StopBit = RxBuf[RxHead+14];
              } 
            }
            //38400
            if((RxBuf[RxHead+8]=='3')&&(RxBuf[RxHead+9]=='8')&&(RxBuf[RxHead+10]=='4')&&\
            (RxBuf[RxHead+11]=='0')&&(RxBuf[RxHead+12]=='0')&&(RxBuf[RxHead+13]==',')&&(RxBuf[RxHead+15]==',')&&(RxBuf[RxHead+17]==0x0d))
            {
              if(((RxBuf[RxHead+14]=='0')||(RxBuf[RxHead+14]=='1'))&&((RxBuf[RxHead+16]=='0')||(RxBuf[RxHead+16]=='1')||(RxBuf[RxHead+16]=='2')))
              {
                NumberCommand = 7; //Задаем номер команды 
                BaudRate = 4; //38400
                ParityBit = RxBuf[RxHead+16];
                StopBit = RxBuf[RxHead+14];
              } 
            }
            //57600
            if((RxBuf[RxHead+8]=='5')&&(RxBuf[RxHead+9]=='7')&&(RxBuf[RxHead+10]=='6')&&\
            (RxBuf[RxHead+11]=='0')&&(RxBuf[RxHead+12]=='0')&&(RxBuf[RxHead+13]==',')&&(RxBuf[RxHead+15]==',')&&(RxBuf[RxHead+17]==0x0d))
            {
              if(((RxBuf[RxHead+14]=='0')||(RxBuf[RxHead+14]=='1'))&&((RxBuf[RxHead+16]=='0')||(RxBuf[RxHead+16]=='1')||(RxBuf[RxHead+16]=='2')))
              {
                NumberCommand = 7; //Задаем номер команды 
                BaudRate = 5; //57600
                ParityBit = RxBuf[RxHead+16];
                StopBit = RxBuf[RxHead+14];
              } 
            }
            //115200
            if((RxBuf[RxHead+8]=='1')&&(RxBuf[RxHead+9]=='1')&&(RxBuf[RxHead+10]=='5')&&\
            (RxBuf[RxHead+11]=='2')&&(RxBuf[RxHead+12]=='0')&&(RxBuf[RxHead+13]=='0')&&(RxBuf[RxHead+14]==',')&&(RxBuf[RxHead+16]==',')&&(RxBuf[RxHead+18]==0x0d))
            {
              if(((RxBuf[RxHead+15]=='0')||(RxBuf[RxHead+15]=='1'))&&((RxBuf[RxHead+17]=='0')||(RxBuf[RxHead+17]=='1')||(RxBuf[RxHead+17]=='2')))
              {
                NumberCommand = 7; //Задаем номер команды 
                BaudRate = 6; //115200
                ParityBit = RxBuf[RxHead+17];
                StopBit = RxBuf[RxHead+15];
              } 
            }
           
             
          } //Конец АТ+UART=
        
        //AT+ROLE?
        if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='R')&&(RxBuf[RxHead+4]=='O')&&\
          (RxBuf[RxHead+5]=='L')&&(RxBuf[RxHead+6]=='E')&&(RxBuf[RxHead+7]=='?')&&\
            (RxBuf[RxHead+8]==0x0d)) {NumberCommand = 8;}
        //AT+PSWD?
        if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='P')&&(RxBuf[RxHead+4]=='S')&&\
          (RxBuf[RxHead+5]=='W')&&(RxBuf[RxHead+6]=='D')&&(RxBuf[RxHead+7]=='?')&&\
            (RxBuf[RxHead+8]==0x0d)) {NumberCommand = 9;}
        //AT+PSWD=
          if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='P')&&(RxBuf[RxHead+4]=='S')&&\
          (RxBuf[RxHead+5]=='W')&&(RxBuf[RxHead+6]=='D')&&(RxBuf[RxHead+7]=='=')) 
          {
            j = 0; //Обнуляем индекс массива Param
            i=RxHead+8; //Задаем начальный индекс
            while(i<RxTile) //Считываем все принятые символы
            {  
              ParamBuf[j] = RxBuf[i]; //Заполняем массим Param
              j++; //Инкремент индексов
              i++;
            }
            NumberCommand = 10; //Задаем номер команды  
          } 
        //AT+VERSION?
        if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='V')&&(RxBuf[RxHead+4]=='E')&&\
          (RxBuf[RxHead+5]=='R')&&(RxBuf[RxHead+6]=='S')&&(RxBuf[RxHead+7]=='I')&&\
             (RxBuf[RxHead+8]=='O')&&(RxBuf[RxHead+9]=='N')&&(RxBuf[RxHead+10]=='?')&&(RxBuf[RxHead+11]==0x0d)) {NumberCommand = 11;}
        //AT+ORGL
        if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='O')&&(RxBuf[RxHead+4]=='R')&&\
          (RxBuf[RxHead+5]=='G')&&(RxBuf[RxHead+6]=='L')&&(RxBuf[RxHead+7]==0x0d)) {NumberCommand = 12;} 
        //AT+HELP
        if((RxBuf[RxHead+2]=='+')&&(RxBuf[RxHead+3]=='H')&&(RxBuf[RxHead+4]=='E')&&\
          (RxBuf[RxHead+5]=='L')&&(RxBuf[RxHead+6]=='P')&&(RxBuf[RxHead+7]==0x0d)) {NumberCommand = 13;} 
        
      }
      RxHead++; 
    }
    if(RxHead == RxTile) {RxHead = 0; RxTile = 0;} //Если дошли до конца то начинаем по новой
     
    CRflag = 0; //Сброс флага принятого символа возврата каретки
  }
  
}

//============================
//==Отправить строку в буфер==
//============================
void SendSTR(unsigned char *data)
{
  unsigned char sym;
  while(*data){ // Отправляем посимвольно всю строку
    sym = *data++;
     while(!(UCSRA & (1<<UDRE))); // Ждем пока освободится передатчик
     UDR = sym; 
     __delay_cycles(110000); //Задержка между посылкой символов 
  }
}

//============================
//==Отправить строку в буфер==
//============================
void SendSTRHELP(char __flash *data1)
{
  unsigned char sym;
  while(*data1){ // Отправляем посимвольно всю строку
    sym = *data1++;
     while(!(UCSRA & (1<<UDRE))); // Ждем пока освободится передатчик
     UDR = sym; 
    // __delay_cycles(11000); //Задержка между посылкой символов 
  }
}

//==========
//==
//==========
void SendCRLF(void)
{
  while(!(UCSRA & (1<<UDRE))); // Ждем пока освободится передатчик
  UDR = 0x0d; // CR
  __delay_cycles(110000); //Задержка между посылкой символов
  while(!(UCSRA & (1<<UDRE))); // Ждем пока освободится передатчик
  UDR = 0x0a; // LF
  __delay_cycles(11000); //Задержка между посылкой символов
}


void SendAnswer(void)
{
  while(RxHead != RxTile) // Перебираем все принятые символы
  {
    while(!(UCSRA & (1<<UDRE))); // Ждем пока освободится передатчик
    UDR = RxBuf[RxHead]; 
    RxHead++;
    __delay_cycles(110000); //Задержка между посылкой символов 
  }
  if(RxHead == RxTile) {RxHead = 0; RxTile = 0;} //Если дошли до конца то начинаем по новой
}


//===================
//==Запись в ЕЕПРОМ==
//===================
unsigned char EEPROM_WR(unsigned char AddrH, unsigned char Data)
{
  unsigned char fl = 0;
  /* Wait for completion of previous write */
  while(EECR & (1<<EEWE))
  ;
  __disable_interrupt();
  /* Set up address and data registers */
  EEAR = AddrH;
  EEDR = Data;
  /* Write logical one to EEMWE */
  EECR |= (1<<EEMWE);
  /* Start eeprom write by setting EEWE */
  EECR |= (1<<EEWE);
 
  
  
  /* Wait for completion of previous write */
  while(EECR & (1<<EEWE))
  ;
  /* Set up address register */
  EEAR = AddrH;
  /* Start eeprom read by writing EERE */
  EECR |= (1<<EERE);
  if (EEDR == Data)
    fl = 1; 
  __enable_interrupt();  
  return fl;
  
  
}

//====================
//==Чтение из ЕЕПРОМ==
//====================
unsigned char EEPROM_READ(unsigned char AddrH)
{
   __disable_interrupt();
  /* Wait for completion of previous write */
  while(EECR & (1<<EEWE))
  ;
  /* Set up address register */
  EEAR = AddrH;
  /* Start eeprom read by writing EERE */
  EECR |= (1<<EERE);
  /* Return data from data register */
   __enable_interrupt(); 
  return EEDR;
}
